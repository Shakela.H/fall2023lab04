// Student Name: Shakela Hossain
// Student ID: 1435730
// Due Date: September 18, 2023
package debugging.assignment;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
